Pokémon TCG Online Property Research & Documentation
====================================================

“Archetypes” are the basic, abstract unit of an “item” in Pokémon TCG
Online. They are used for cards, products, items, and just about
anything that a player can possess in the game.

Archetypes consist of GUID and a set of attribute:value pairs, and can
be split into three broad categories:

-  Cards
-  Products / Items
-  Miscellaneous

This document describes the various architecture attributes that are
observable within the Pokémon TCG Online game client. They describe
properties and attributes of Pokémon cards, products, and miscellaneous
inventory items that players may hold.

Each section is titled as:
   ``number - name (canonicalName) [type]``

Where the number is the ID of each attribute, and the name is either a
deduced or decompiled name for the property. In some cases where I have
decided on a name that is not the canonical name for the property, and a
canonical name is available, that name will be present as
``(canonicalName)`` in the section title.

The type of the field will be denoted as ``[type]``, where type is any
one of:

-  str - string
-  int - number
-  bool - true/false flag
-  I18N - string that may contain ``$$$token$$$`` lookup codes that can
   be decoded to specific locales.
-  JSON - JSON data encoded directly as a string. See each property for
   details on the specific shape of the data.

And in some cases, the field will be documented as ``#UNREAD``, which
signifies that although the attribute is present, the Pokémon TCG Online
game client did not, at the time of investigation, actually attempt to
read or make use of that value.

Global Properties
-----------------

Miscellaneous archetypes only set property 10140 (name), so global
properties are effectively ones that include everything common between
Cards and Products.

10020 - assetName (image) [str]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Refers to the image asset name for this archetype. Not always set, this
overrides the default lookup path of ``${archKey}/${cardNo}``

Examples:

-  Cards: ``'177xy', '148it', '114_gold', '017a'``
-  Products:
   ``'skyguardianxy10pcd', 'packs/SM8BoosterBase', 'bw2toxictricks',``
   ``'packs/SM8Booster6Card'``

10090 - hidden [bool]
~~~~~~~~~~~~~~~~~~~~~

Referred to by the game as “hidden”, always set to False for cards, and
Set to False when set for products.

10140 - name (pieName) [I18N]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LocalizableText representing the name of the card or product.

Examples:

-  Cards:
   ``$$$com.direwolfdigital.cake.data.archetypes.pokemon.LeafeonGX.Name$$$``
-  Products: ``$$$product.coin.ChikoritaCoin.name$$$``

10190 - setSortOrder [int]
~~~~~~~~~~~~~~~~~~~~~~~~~~

Arbitrary digit representing the general sort order of the expansion/set
the card/product belongs to, if set. Not always the same for all
archetypes belonging to a set, but usually is.

Exceptions:

-  XY11 has some products tagged as 58 (XY10) instead of its own 59.
-  TK8A has some products tagged as 53 (TK8B), instead of its own 52.
-  TwentiethAnn has two products tagged as 56 (XY9), instead of its own
   57.

200550 - rarity [str]
~~~~~~~~~~~~~~~~~~~~~

Represents the rarity of an item.

Values:

-  Products: ``{'RareHolo', 'medium', 'PromoExclusive'}``
-  Cards:
   ``{'Common', 'RarePrime', 'Rare', 'Legendary', 'RareRainbow',``
   ``'RarePromo', 'Uncommon', RareUltra', 'RareHolo', 'Shining', 'RareSecret',``
   ``'Prism', 'RareHoloGX', 'RareHoloEX', 'Ace', 'BreakRare'}``

For cards, the values are interpreted directly as the card’s symbolic
rarity. Some cards without symbols, e.g. Dragon Vault, have this field
set to “RarePromo”.

For products:

-  “PromoExclusive” for certain deckboxes, coins, sleeves, etc.
-  “RareHolo” for “Rare Holo Chest”.
-  “medium” for many coins, deckboxes, sleeves, etc.

200580 - archKey (setKey) [str]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The name of the set this archetype is associated with. This not always
set, but since archetypes are always distributed in bundles associated
with a set, it is always possible to identify the archKey of an
archetype.

When this field is set, it is never different from the bundle it is
distributed with.

Values:

::

   {'AvatarItems', 'BW1', 'BW10', 'BW11', 'BW2', 'BW3', 'BW4', 'BW5', 'BW6', 'BW7',
    'BW8', 'BW9', 'BW_Energy', 'COL', 'DM', 'DV', 'Free_Energy', 'HGSS1', 'HGSS2',
    'HGSS3', 'HGSS4', 'HGSS_Energy','Promo_BW', 'Promo_HGSS', 'Promo_SM',
    'Promo_XY', 'RSP', 'RewardItems', 'SL', 'SM1', 'SM2', 'SM3', 'SM4', 'SM5',
    'SM6', 'SM7', 'SM8', 'SM9', 'SM_Energy', 'TATM', 'TK10A', 'TK10B', 'TK5A',
    'TK5B', 'TK6A', 'TK6B', 'TK7A', 'TK7B', 'TK8A', 'TK8B', 'TK9A', 'TK9B',
    'TwentiethAnn', 'XY0', 'XY1', 'XY10', 'XY11', 'XY12', 'XY2', 'XY3', 'XY4',
    'XY5', 'XY6', 'XY7', 'XY8', 'XY9', 'XY_Energy'}

202220 - dateAdded [int] #UNREAD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From around the time after SM4 was released to as of now (SM9), this
reflects the time this archetype was added to PTCGO, in milliseconds.

Example:

-  1525365900000 is 2018-05-13.

The PTCGO client does not read this value.

Card Properties
---------------

Global Card Properties
~~~~~~~~~~~~~~~~~~~~~~

10000 - guid [str]
^^^^^^^^^^^^^^^^^^

GUID of the card. Always set, and always matches the Archetype property
of the same name.

Example:

-  (‘2fe26716-3c2e-422a-8553-3db32fe5d3b2’, ‘SM9 #112 Mimikyu [ph]’)

200300 - cardType [str]
^^^^^^^^^^^^^^^^^^^^^^^

The type of Pokémon card. Always set for cards.

Values:

::

   {'Energy', 'Pokemon', 'LegendHalf', 'TrainerCard'}

200310 - rulesText [I18N] {Trainers & Energy}
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

LocalizableText string that describes the special rules for this Trainer
or Energy card. Special rules for Pokémon are always described in
`200740 - abilities [JSON] <#abilities-json>`__ instead.

Takes the form of a LocalizableText string, e.g.:

   ``$$$com.direwolfdigital.cake.rules.abilities.trainers.SM9.BlackMarketPrismStar.GameText$$$``

Which localizes to:

   “When a {D} Pokémon (yours or your opponent’s) that has any {D}
   Energy attached to it is Knocked Out by damage from an opponent’s
   attack, that player takes 1 fewer Prize card.\n\nWhenever any player
   plays an Item or Supporter card from their hand, prevent all effects
   of that card done to this Stadium card.”

200360 - teamAffinity [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Represents a list of team affiliations this card has, which is usually
just one. However, “HyperGear” is also considered a team here, so it
makes an appearance.

Values:

::

   {['TeamFlare'],
    ['TeamPlasma'],
    ['TeamMagma'],
    ['TeamFlare', 'HyperGear'],
    ['TeamAqua']}

200400 - leagueCard [bool]
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set true when this card is an Organized Play card. This almost always
implies that ‘issue’ is “op”.

Exceptions: \* 8a8366e3-9ba5-4b36-af7f-613731a445a6 BW11 #109 Bianca
[ph]

200520 - \__unusedBoolA [bool] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :hidden:

   200520


Unknown boolean, not used in any way by the PC game client.

There is seemingly no compelling overall pattern, but it appears to be
set for cards in the following sets:

-  HGSS1 (Some trainers and energies)
-  HGSS_Energy
-  HGSS2 (Some trainers)
-  COL (Energies)
-  BW1 (Some Pokémon, Some Trainers, and Energies)
-  BW_Energy
-  TK5B (Energies, some Pokémon, some items)
-  TK5A (Energies, some Pokémon, some items)
-  Promo_BW (Various Pokémon, OP trainers)
-  BW2
-  BW3
-  XY_Energy
-  XY1 (Energy)
-  RSP
-  TwentiethAnn (Energy)
-  Promo_XY
-  SM_Energy
-  TK10A (Energy)
-  TK10B (Energy)

A full list can be found in :doc:`200520`.

200560 - subRarity [str]
^^^^^^^^^^^^^^^^^^^^^^^^

Effective rarity set for certain cards; contrast with contrast with
`200550 - rarity [str] <#rarity-str>`__.

It is set especially for Dragon Vault cards which are otherwise
“RarePromo”. Also set for a handful of “RareSecret” cards from HGSS1 and
XY7 (five in total) whose rarity is otherwise “RareSecret”.

Possible Values:

::

   {'RareHolo', 'Uncommon', 'RareUltra', 'Rare', 'Common'}

Notable Examples:

-  ``('HGSS1 #123 Gyarados [std]', 'RareSecret', 'RareHolo')``
-  ``('HGSS1 #123 Gyarados [ph]', 'RareSecret', 'RareHolo')``
-  ``('XY7 #98 MRayquazaEX [std]', 'RareSecret', 'RareUltra')``
-  ``('XY7 #96 PrimalKyogreEX [std]', 'RareSecret', 'RareUltra')``
-  ``('XY7 #97 PrimalGroudonEX [std]', 'RareSecret', 'RareUltra')``

200610 - foilEffect [str]
^^^^^^^^^^^^^^^^^^^^^^^^^

For foil cards, this describes the visual effect of the foil.

Values:

::

   {'BWEtch', 'LunalaEtch', 'SunPillar', 'FlatSilver', 'None', 'SunBeam',
   'Squares', 'AngledPillars', 'Galaxy', 'SunLava', 'XYEtch', 'SolgaleoEtch', 
   'Rainbow', 'Cosmos', 'Tinsel', 'EtchedSunPillar', 'Cracked_Ice'}

It can be unset (this is not a Holo card), or it can be set literally to
the string ‘None’. It is always set if `200620 - foilMask
[str] <#foilmask-str>`__ is set.

200620 - foilMask [str]
^^^^^^^^^^^^^^^^^^^^^^^

For foil cards, this describes the foil mask type. It is always set if
`200610 - foilEffect [str] <#foileffect-str>`__ is set. Similarly, it
can also feature the literal string ‘None’.

Values:

::

   {'None', 'Reverse', 'Etched', 'Holo', 'Thatch'}

200630 - shortName (cardName) [str]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is an unformatted English internal name for each card.

Examples:

-  JolteonGX
-  ZoroarkBREAK
-  ShiningLugia
-  DittoPrismStar
-  ProfessorOaksNewTheory
-  TeamAquasMuk

200740 - abilities [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^

JSON list of abilities associated with this card. All of the subfield
names are taken directly from the original source; as opposed to
reconstructed or guessed like most of the property names. As such, they
should be fairly self-explanatory.

Example for Pikachu & Zekrom:

::

   [
     {
       "cost": {
         "Lightning": 3
       },
       "damage": 150,
       "title": "$$$com.direwolfdigital.cake.rules.abilities.attacks.SM9.PikachuZekromGX.FullBlitz.Title$$$",
       "gameText": "$$$com.direwolfdigital.cake.rules.abilities.attacks.SM9.PikachuZekromGX.FullBlitz.GameText$$$",
       "abilityID": "b0959832-256c-44c2-aff8-4e6f2f6088dc",
       "amountOperator": "",
       "abilityType": "Attack",
       "conditionExceptions": [],
       "sortOrder": 1,
       "costPrefixes": [],
       "costSuffixes": []
     },
     {
       "cost": {
         "Lightning": 3
       },
       "damage": 200,
       "title": "$$$com.direwolfdigital.cake.rules.abilities.attacks.SM9.PikachuZekromGX.TagBoltGX.Title$$$",
       "gameText": "$$$com.direwolfdigital.cake.rules.abilities.attacks.SM9.PikachuZekromGX.TagBoltGX.GameText$$$",
       "abilityID": "db5be543-02ef-475d-bea9-c581a86d9205",
       "amountOperator": "",
       "abilityType": "Attack",
       "conditionExceptions": [],
       "sortOrder": 2,
       "buttonOverride": "abilityGX",
       "costPrefixes": [],
       "costSuffixes": [
         "iconPlus"
       ]
     },
     {
       "title": "$$$playmat.directaction.label.play$$$",
       "gameText": "$$$playmat.prompt.sm7_83.fallingstar_bench$$$",
       "abilityID": "03831a39-7e80-405d-b959-70df2fa96c3f",
       "abilityType": "PlayAbility"
     }
   ]

The third ability there represents a more abstract ability; the gameText
is “Play this Pokémon onto your Bench.”

Example for an Energy card (Sun & Moon Grass Energy):

::

   [
     {
       "title": "$$$playmat.directaction.label.play$$$",
       "gameText": "$$$playmat.prompt.sm8_165.mountainpass.play$$$",
       "abilityID": "807d7113-9269-4443-ae78-22d61a37ac00",
       "abilityType": "PlayAbility"
     }
   ]

Again, the ability is fairly abstract. The title is “Play” and the
gameText is “Play this card.”

Example for a Trainer card:

::

   [
     {
       "title": "$$$playmat.directaction.label.play$$$",
       "gameText": "$$$playmat.prompt.sm8_165.mountainpass.play$$$",
       "abilityID": "3b95e3b1-965e-42bb-bf0c-4ac94e2fb4dd",
       "abilityType": "PlayAbility"
     }
   ]

Again, the ability is simply the generic one to “Play this card.”

200780 - cardNo [int]
^^^^^^^^^^^^^^^^^^^^^

Card sort number. Presently, it can be [1-236] inclusive.

200790 - cardNoText [str]
^^^^^^^^^^^^^^^^^^^^^^^^^

For cards with non-numerical card numbers, represents the text on the
card where where the number usually appears, such as on Promo cards,
Yellow A Alternates, etc.

Examples:

-  XY67a
-  HGSS17
-  RC5/RC25
-  SL5

200871 - issue [str] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Represents the “issue” of the card.

-  std: Standard issue card.
-  ph: Parallel Holo card (“reverse holo” informally.)
-  op: Organized Play promotional card.
-  yaa: Yellow A Alternate.
-  pcd: Pre-Constructed Deck variant.
-  alt: Alternate promotional card. (Booster blister packs, etc.)
-  ref: Unknown. Set only for Promo BW #BW54 Pikachu.

201000 - fullArt [bool]
^^^^^^^^^^^^^^^^^^^^^^^

True if the card is a “Full Art” style card.

The difference between a standard print and a “full art” print can be a
little arbitrary at times, considering some of the art of the {GX}
cards, but this generally refers to the Full Art cards that appear
outside of the standard card ordering at the end of the set. It
coincides directly with which such cards show up if you search for “Full
Art” in Pokemon TCG Online.

201620 - \__unusedTimeA [int] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :hidden:

   201620

This timestamp appears to be an abandoned way of setting the release
date. It is set on, generally, one card by name for every expansion,
from the HGSS1 era through the BW10 set. Not all sets have a card that
carries this property, and it isn’t always set correctly.

See :doc:`201620` for more details.

201710 - originalPrintID [str]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

GUID of the original printing of this card. In this case, a “printing”
includes cards with the same name and same (or only slightly modified)
rules text, but can have different art and come from different sets.

Notably, most [ph] issues have the GUID of their [std] sibling. Some
[std] cards have their own GUID set if they are the first printing.

However, not all GUIDs set here are actually in the game. In some cases,
this appears to be because the original print is a pre-HGSS1 card that
isn’t actually contained within Pokémon TCG Online, but in other cases
it’s difficult to account for.

For example:

-  (‘HGSS1 #2 Azumarill [std]’, ‘5007a183-73b6-4dcc-8f53-28b1d4cfa95a’)
-  (‘HGSS1 #2 Azumarill [ph]’, ‘5007a183-73b6-4dcc-8f53-28b1d4cfa95a’)

HGSS1 Azumarill does not to my knowledge have any other printings, but
the GUIDs for these cards are:

-  (‘HGSS1 #2 Azumarill [std]’, ‘e995d74e-5766-11e1-81f9-782bcb9e3ead’)
-  (‘HGSS1 #2 Azumarill [ph]’, ‘e995fe5e-5766-11e1-81f9-782bcb9e3ead’)

It’s possible that this GUID field used to refer to an abstract print
that was neither the [std] nor [ph] issues and at some point the game
re-organized the way it issues cards, but I can’t tell.

Newer sets appear to be mostly consistent, except with reprint IDs for
old trainer cards:

-  (‘SM7 #129 EnergySwitch [std]’,
   ‘1703ddd1-6a5f-4a96-894b-8583c20f6011’)
-  (‘SM7 #129 EnergySwitch [ph]’,
   ‘1703ddd1-6a5f-4a96-894b-8583c20f6011’)
-  (‘SM7 #147 Switch [std]’, ‘8ce3d65c-da9c-4d0f-a6d4-ecdea65e9152’)
-  (‘SM7 #147 Switch [ph]’, ‘8ce3d65c-da9c-4d0f-a6d4-ecdea65e9152’)

However, even in newer sets there are still some missing GUIDs that one
would expect to see in the database:

-  (‘Promo_SM #SM17 LunalaGX [std]’,
   ‘e941c375-545d-47bf-90b9-7f159c264ba0’,
   ‘5f345ce9-9893-4ca3-8786-3718bae791bc’)
-  (‘SM5 #172 LunalaGX [std]’, ‘7ac6a4e8-302b-46e4-adf3-1cf6e3e3a9ed’,
   ‘5f345ce9-9893-4ca3-8786-3718bae791bc’)
-  (‘Promo_SM #SM16 SolgaleoGX [std]’,
   ‘52096038-819f-4099-9d91-6ab043f617d8’,
   ‘67c1341b-9226-420d-9230-d316f77c50fd’)
-  (‘SM5 #173 SolgaleoGX [std]’, ‘192f7feb-77c5-47ff-b926-d736ed435073’,
   ‘67c1341b-9226-420d-9230-d316f77c50fd’)

It is probably wisest to think of this field as a fingerprint/ID to
identify sibling prints; as of SM9 there are 1549 entries not in the
database, referencing 746 unique missing GUIDs.

201730 - containedInDecks [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

JSON list of the preconstructed theme decks this card is part of. The
use of this field was apparently discontinued prior to the Sun & Moon
set. It’s set on cards from HGSS1 through Promo_SM, excluding SM1 and
beyond.

Examples:

-  (‘XY11 #103 NinjaBoy [std]’, [‘gearsoffirexy11pcd’,
   ‘ringoflightningxy11pcd’])
-  (‘BW7 #113 Skitty [ph]’, [‘basicorange’, ‘bw7coldfire’, ‘ella’,
   ‘tutorialdecks’])
-  (‘BW7 #129 GreatBall [ph]’, [‘bw7coldfire’, ‘bw7iceshock’,
   ‘bwbasicblue’, ‘calvin’, ‘tutorialdecks’])
-  (‘XY3 #104 StrongEnergy [ph]’, [‘daniel’, ‘ella’, ‘juji’,
   ‘zygardeexsummer2016’])

Decks referenced:

-  Character Decks:
   ``{'ali', 'brittney', 'calvin', 'cammie', 'daniel', 'ella', 'grayson', 'juji',``
   ``'kendall', 'logan', 'mick', 'nathan', 'newAliDeck', 'newCalvinDeck',``
   ``'newEllaDeck', 'newOtisDeck', 'newPlayerDeck', 'newZachDeck', 'otis',``
   ``'penelope', 'rika', 'tyson', 'zach'}``

-  New Player Decks:
   ``{'basicblue', 'basicgreen', 'basicorange', 'basicred', 'basicyellow',``
   ``'bwbasicblue', 'bwbasicgreen', 'bwbasicred', 'xybasicblue', 'xybasicgreen',``
   ``'xybasicred'}``

..

   **Note**: This seems to exclude “Power Relay” and “XY - Basic
   Yellow”.

-  HGSS Decks:
   ``{'hgss1emberspark', 'hgss1growthclash', 'hgss1mindflood', 'hgss2chaoscontrol',``
   ``'hgss2steelsentinel', 'hgss3daybreak', 'hgss3nightfall', 'hgss4royalguard',``
   ``'hgss4verdantfrost', 'colrecon', 'colretort'}``

-  BW Decks:
   ``{'bw1blueassault', 'bw1greentornado', 'bw1redfrenzy', 'bw2powerplay',``
   ``'bw2toxictricks', 'bw3fastdaze', 'bw3furiousknights', 'bw4explosiveedge',``
   ``'bw4voltagevortex', 'bw5raiders', 'bw5shadows', 'bw6dragonsnarl',``
   ``'bw6dragonspeed', 'bw7coldfire', 'bw7iceshock', 'bw8plasmaclaw',``
   ``'bw8plasmashadow', 'bw9frostray', 'bw9psycrusher', 'mindwipe', 'solarstrike'}``

-  XY Decks:
   ``{'xy0card3', 'xy0card8', 'xy0card12', 'xy1destructionrush',``
   ``'xy1resilientlife', 'xy2helioliskdeck', 'xy2meowsticdeck', 'pangoroxy3deck',``
   ``'sylveonxy3deck', 'earthspulse', 'oceanscore', 'aurorablast', 'stormrider',``
   ``'irontide', 'stoneheart', 'burningspark', 'nightstriker', 'waveslasher',``
   ``'electriceye', 'battlerulerxy10pcd', 'skyguardianxy10pcd',``
   ``'gearsoffirexy11pcd', 'ringoflightningxy11pcd', 'mewtwomayhemxy12pcd',``
   ``'pikachupowerxy12pcd'}``

..

   **Note**: Seems to exclude XY4 decks “Burning Winds” and “Bolt
   Twister”

-  Trainer Kits:
   ``{'nighthunter', 'xytrainerkit', 'tk7', 'tk8', 'tk9', 'tk10'}``

..

   **Note**: Excludes HS Trainer Kit and earlier. Does not reference the
   newest Alolan Ninetales Trainer Kit.

-  Misc Decks:
   ``{'mcdpromo', 'tutorialdecks', 'placeholder', 'xerneasexsummer2016',``
   ``'yveltalexsummer2016', 'zygardeexsummer2016', 'rallyingcry'}``

202080 - foilIntensity [int]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An integer that the game uses as “foil intensity”, values:
``{1, 3, 5, 8, 15, 200, 201}``.

The overwhelming number of cards use ‘201’ (10729), some use ‘5’ (55),
and all others have one or two uses.

202110 - legalityDate [int] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Unix time (in milliseconds) when this promotional card becomes
tournament legal. It is not used by the PTCGO game client.

Not always consistently set for non-expansion cards.

Examples:

-  Promo_SM #SM168 PikachuZekromGX [std]: 1550217600000 (2019-02-15)
-  Promo_SM #SM178 MelmetalGX [std]: 1551427200000 (2019-03-01)

202200 - tags [JSON]
^^^^^^^^^^^^^^^^^^^^

JSON array of tags that apply to this card. Possible tags are:

-  League
-  TAG
-  PrismStar
-  Shining
-  YellowA
-  UltraBeast

Examples:

-  (‘SM4 #63a/111 GuzzlordGX [yaa]’, [‘UltraBeast’, ‘YellowA’])
-  (‘SM6 #80 Guzzlord [op]’, [‘UltraBeast’, ‘League’])
-  (‘SM2 #46 Oricorio [op]’, [‘League’])
-  (‘SM2 #125a/145 FieldBlower [yaa]’, [‘YellowA’, ‘League’])

Pokémon & LegendHalf Properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These properties are common and exclusive to ``cardType = Pokemon`` and
``cardType = LegendHalf`` Archetypes.

200260 - familyID [int]
^^^^^^^^^^^^^^^^^^^^^^^

Numerical ID representing this Pokémon’s “Family”. For instance, Pikachu
family is 27, which includes Pikachu, Raichu, RaichuBREAK, and
AlolanRaichu.

It does not appear to include e.g. Pikachu & Zekrom, or Flying/Surfing
Pikachu.

See `familyMap.md <familyMap.html>`__ for the full list as of SM9.

200280 - previousEvolution [I18N]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Localized string describing the previous evolution of this Pokémon.

Examples:

-  (‘SM9 #75 Omanyte [std]’,
   ``$$$com.direwolfdigital.cake.data.archetypes.trainercards.UnidentifiedFossil.Name$$$``)
-  (‘SM9 #55 Nidorina [std]’,
   ``$$$com.direwolfdigital.cake.data.archetypes.pokemon.NidoranFemale.Name$$$``)
-  (‘SM9 #18 Rapidash [std]’,
   ``$$$com.direwolfdigital.cake.data.archetypes.pokemon.Ponyta.Name$$$``)

200430 - burnAmount [int]
^^^^^^^^^^^^^^^^^^^^^^^^^

Always set to 20. Represents how much burn damage this Pokémon takes.

200490 - hp [int]
^^^^^^^^^^^^^^^^^

Pokémon’s HP. Can be 30 to 300, inclusive.

200540 - stage [str]
^^^^^^^^^^^^^^^^^^^^

Pokémon’s stage of evolution.

::

   ['Basic', 'Break', 'Restored', 'Stage1', 'Stage2']

200570 - types [JSON]
^^^^^^^^^^^^^^^^^^^^^

JSON array of this Pokémon’s types.

Possible component values are:

::

   ['Colorless', 'Darkness', 'Dragon', 'Fairy', 'Fighting', 'Fire', 'Grass', 'Lightning', 'Metal', 'Psychic', 'Water']

Examples:

-  Singly-typed Pokémon: ``['Fire']``
-  Doubly-typed Pokémon: ``['Water', 'Fire']``

200590 - weaknesses [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^^

JSON array of types this Pokémon is weak to.

Examples:

-  None
-  [‘Fire’]
-  [‘Fighting’, ‘Lightning’]

200600 - resistance [str]
^^^^^^^^^^^^^^^^^^^^^^^^^

The type, if any, this Pokémon is resistant to.

``{'Colorless', 'Psychic', 'Lightning', 'NoColor', 'Metal', 'Darkness', 'Water', 'Fighting', None}``

200640 - evolvesFrom [str]
^^^^^^^^^^^^^^^^^^^^^^^^^^

Similar to previousEvolution, but this uses the internal (English)
shortname, and not the I18N string.

Examples:

-  (‘SM9 #75 Omanyte [std]’, ‘UnidentifiedFossil’)
-  (‘SM9 #55 Nidorina [std]’, ‘NidoranFemale’)
-  (‘SM9 #18 Rapidash [std]’, ‘Ponyta’)

200650 - resistanceOp [char]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The type of resistance this Pokémon has. Always set to ‘-’.

200660 - weaknessOp [char]
^^^^^^^^^^^^^^^^^^^^^^^^^^

The type of weakness this Pokémon has. Set to either ‘x’ for
multiplicative or ‘+’ for aditive.

200800 - retreatCost [int]
^^^^^^^^^^^^^^^^^^^^^^^^^^

Retreat cost of this Pokémon, from 0 to 4.

200820 - weaknessAmt [int]
^^^^^^^^^^^^^^^^^^^^^^^^^^

The weakness value for this Pokémon, either 0, 2, or 10.

10 is used for +10, 2 is used for 2x.

200830 - resistanceAmt [int]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The resistance value for this Pokémon, either 0 or 20.

201010 - exPokemon [bool]
^^^^^^^^^^^^^^^^^^^^^^^^^

True if this is a Pokémon ex. The shortName for this Pokémon always ends
in ‘EX’, and vice versa.

201030 - legendPokemon [bool]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

True if this is a Pokémon Legend. The cardType of this card is always
LegendHalf, and the shortName always ends with “LEGEND”.

201610 - numPokeTools [int] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Represents how many PokeTools this card can utilize. Set to ‘2’ for
cards with the θ Double trait from the Ancient Origins set (XY7).

201750 - fossilBase [str] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This value is unread by the PTCGO client. This value is set on BW3
through XY11 era cards that evolve from fossils. It appears to be fully
redundant with the “evolvesFrom” field in the cases where it is set.

Values:

::

   {'JawFossil', 'SailFossil', 'OldAmberAerodactyl', 'ClawFossilAnorith', 'ArmorFossilShieldon', 'DomeFossilKabuto', 'PlumeFossil', 'RootFossilLileep', 'HelixFossilOmanyte', 'CoverFossil'}

202120 - gxAbilities [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^^^

JSON array of GUIDs that uniquely identify specific GX attacks that this
Pokémon has. The GUID for attacks can be found within the ``abilities``
property, by the ``abilityID`` subproperty.

In most cases, this GUID always matches the GUID of their GX attack(s)
in the ``abilities`` property. However, there are some exceptions:

-  (‘SM5 #157 LeafeonGX [std]’, ‘5204ad57-f181-4288-bb91-8342f46bda2d’)
-  (‘SM5 #139 LeafeonGX [std]’, ‘83c70fe3-8eea-4d0e-b011-e42c005e82a6’)
-  (‘Promo_SM #SM176 EeveeGX [std]’,
   ‘7bc4c374-1b58-4487-8cfb-7e2d0bd82be4’)
-  (‘Promo_SM #SM175 EeveeGX [std]’,
   ‘a2852919-63d0-4b7d-9b5d-60333cd8e76b’)

These cards reference GX Abilities that are not described anywhere in
the game at present; they may reference a different “version” of the
ability that was not updated properly.

Trainer Card Properties
~~~~~~~~~~~~~~~~~~~~~~~

200200 - pokeToolText [I18N]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Localizable String for Pokémon Tool cards, though it’s not set on every
PokeTool card. It can be set to several values:

-  ``$$$common.rulesText.trainercards.poketoolshort$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.BW3.trainercards.Eviolite_BW3_91.PokeToolText$$$``
-  ``$$$common.rulesText.trainercards.flaretoolshort$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.BW6.trainercards.GiantCape_BW6_114.PokeToolText$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.BW6.trainercards.RescueScarf_BW6_115.PokeToolText$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.BW3.trainercards.RockyHelmet_BW3_94.PokeToolText$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.bw4.trainercards.cilan_bw4_87.poketooltext$$$``
-  ``$$$com.direwolfdigital.cake.data.archetypes.BW5.trainercards.DarkClaw_BW5_92.PokeToolText$$$``

All except the “flaretool” string above resolve to the same text:

   “Attach a Pokémon Tool to 1 of your Pokémon that doesn’t already have
   a Pokémon Tool attached to it.”

The “flaretool” string resolves to an empty string.

(Interestingly, the two cards that use the “flaretool” version of the
text are ``XY4 #98 JammingNet [std]`` and ``XY4 #97 HeadRinger [std]``,
which are cards that I suspect are v2s that supersede an earlier version
of that card; there is a “Corrections” map that TPCi/DWD use to retire
cards and replace them with new versions; only three cards are on this
map and these cards are both on that list.)

200270 - trainerType [str]
^^^^^^^^^^^^^^^^^^^^^^^^^^

The type of Trainer Card. One of:

::

   {'Supporter', 'Stadium', 'PokemonToolF', 'PokemonTool', 'Item'}

201970 - \__unusedBoolB [bool] #UNREAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Unused boolean, only present and set True for two cards:

-  ‘HGSS3 #72 Defender [std]’
-  ‘HGSS3 #72 Defender [ph]’

Energy Card Properties
~~~~~~~~~~~~~~~~~~~~~~

200970 - specialEnergy [bool]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set to True if the Energy card is a non-basic kind.

Set of ``shortName`` values for cards with this property set:

::

   {'BeastEnergyPrismStar', 'BlendGRPDEnergy', 'BlendWLFMEnergy', 'BurningEnergy', 'CounterEnergy', 'DangerousEnergy', 'DoubleAquaEnergy', 'DoubleColorlessEnergy', 'DoubleDragonEnergy', 'DoubleMagmaEnergy', 'FlashEnergy', 'HerbalEnergy', 'MemoryEnergy', 'MysteryEnergy', 'PlasmaEnergy', 'PrismEnergy', 'RainbowEnergy', 'RescueEnergy', 'ShieldEnergy', 'SpecialDarknessEnergy', 'SpecialMetalEnergy', 'SplashEnergy', 'StrongEnergy', 'SuperBoostEnergyPrismStar', 'UnitEnergyFDY', 'UnitEnergyGRW', 'UnitEnergyLPM', 'WarpEnergy', 'WonderEnergy'}

201040 - energyProvided [JSON]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

JSON structure describing the energy provided by an Energy card. See
`200570 - types [JSON] <#types-json>`__ for component values.

Examples:

-  ``{"options": [[]]}``
-  ``{"options": [['Metal']]}``
-  ``{"options": [['Colorless', 'Colorless']]}``

The subfield was named perhaps with different loadouts of energy
provided in mind, but PTCGO only has cards which offer one “option”.

Product Properties
------------------

-605044899 -
~~~~~~~~~~~~

.. _section-1:

10060 -
~~~~~~~

.. _section-2:

10200 -
~~~~~~~

.. _section-3:

10300 -
~~~~~~~

.. _section-4:

10510 -
~~~~~~~

10540 - productType [str]
~~~~~~~~~~~~~~~~~~~~~~~~~

The presence of this field indicates that it is a product. The type is
always set to one of:

::

   ['Bundles', 'Coins', 'Currency', 'DeckBox', 'Decks',
    'Miscellaneous', 'Packs', 'Proofset', 'Singles', 'Sleeve']

.. _section-5:

10640 -
~~~~~~~

.. _section-6:

200670 -
~~~~~~~~

.. _section-7:

200950 -
~~~~~~~~

.. _section-8:

201250 -
~~~~~~~~

.. _section-9:

201260 -
~~~~~~~~

.. _section-10:

201270 -
~~~~~~~~

.. _section-11:

201280 -
~~~~~~~~

.. _section-12:

201370 -
~~~~~~~~

.. _section-13:

201507 -
~~~~~~~~

.. _section-14:

201640 -
~~~~~~~~

.. _section-15:

201890 -
~~~~~~~~

.. _section-16:

201930 -
~~~~~~~~

.. _section-17:

201940 -
~~~~~~~~

.. _section-18:

201941 -
~~~~~~~~

.. _section-19:

202060 -
~~~~~~~~

.. _section-20:

202250 -
~~~~~~~~

.. _section-21:

2012906479 -
~~~~~~~~~~~~
