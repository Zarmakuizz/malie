PTCGO Errata
============

This document lists known errata for Pokemon: Trading Card Game Online
extracted images and how they differ from physical cards.


PTCGO Unique Prints
-------------------

These cards are either PTCGO-Only, or differ in some way from the
corresponding physical print.

-  Champions Festival RSP:S14 8 [op]
   -  Red Star Promo vs Black Star Promo w/ Worlds logo
-  Champions Festival RSP:S15 02 [op]
   -  Red Star Promo vs Black Star Promo w/ Worlds logo
-  Champions Festival RSP:S16 01 [op]
   -  Red Star Promo vs Black Star Promo w/ Worlds logo


Digital Errata Prints
---------------------

These cards received a corrected PTCGO print, but a corresponding
physical errata print was never issued:

-  Pokémon Catcher EPO:95
   -  PTCGO print writes “ERRATA APPLIED” to the top border.
-  Shield Energy PRC:143
-  Onix UL:56
-  Cyrus {*} UPR:120
-  Defender UD:72
-  PlusPower UL:80
   -  Physical print states "Attaches to the Pokémon".
-  Jamming Net Team Flare Hyper Gear PHF:98
-  Fighting Stadium FFI:90
-  Unown TM:51
-  Exp. Share DRV:18
   -  Physical print lacks the “Tool” rule box.
-  Charizard PLS:136
   -  Scorching Fire was printed as [F][C][C][C][C] instead of the correct [R][C][C][C][C]


Errata Prints
-------------

These cards are errata printings that have physical counterparts:

-  Electrode EVO:40
-  Galvantula STS:42
