#!/usr/bin/env python3

from lxml import html
import requests
import datetime
import re
import pprint

base_url = 'https://pkmncards.com/'
seed_url = 'https://pkmncards.com/card/alakazam-base-set-bs-1/'
_re = {
    'th': re.compile(' – (.+?) – (.+?)$'),
    'sef': re.compile('(.+?) – (.+?) $'),
    'ri': re.compile(' – (.+?)/(.+?) – (.+?)$'),
    'title': re.compile('^(.+?) \((.+) ([\S]+?) (.+?)\)$'),
}

def bulbapedia_link(jcard):
    # https://bulbapedia.bulbagarden.net/wiki/Alakazam_(Base_Set_1)
    url = 'https://bulbapedia.bulbagarden.net/wiki/%s_(%s_%s)' % (
        jcard['cdata']['name'],
        jcard['sdata']['set_name'].replace(' ', '_'),
        jcard['cdata']['sort_no'])
    return url

def tat_links(jcard):
    url = 'https://www.trollandtoad.com/products/search.php?search_category=&search_words=%s+%s%%2F%s' % (
              jcard['cdata']['name'],
              jcard['cdata']['sort_no'],
              jcard['sdata']['ncards'])
    page = requests.get(url)
    tree = html.fromstring(page.content)
    results = tree.xpath("//div[contains(concat(' ', normalize-space(@class), ' '), ' search_result_wrapper ')]")
    links = []
    for result in results:
        lnode = result.xpath('div[2]/div[2]/h2/a')[0]
        txt = lnode.text_content()
        link = {'url': lnode.attrib['href'],
                'txt': txt}
        if "Reverse Holo" in txt:
            link['flag'] = 'RH'
        if "1st Edition" in txt:
            link['flag'] = '1E'
        links.append(link)
    return links

def get_card(url):
    # Fetch Data
    page = requests.get(url)
    tree = html.fromstring(page.content)
    nexturl = tree.xpath('//div[contains(@class, "card-nav")]/a[2]/@href')[0]
    text = tree.xpath('//div[contains(@class, "text")]')[0]

    meta = {
        'url': url,
        'nexturl': nexturl,
        'retrieved': datetime.datetime.now().isoformat(),
    }

    cdata = {
        'img': tree.xpath('//div[contains(@class, "scan")]/a/@href')[0],
    }

    sdata = { }
    udata = { 'pkmncards': url }


    # e.g. "Alakazam (Base Set BS 1)"
    # %NAME% (%SET% %SETID% %NO%)
    title = tree.xpath('//h1/span/text()')[0]
    tmpre = _re['title'].match(title)
    cdata['name_'] = tmpre.group(1)
    sdata['set_name_'] = tmpre.group(2)
    sdata['set_sn'] = tmpre.group(3)
    cdata['sort_no'] = tmpre.group(4)

    # Let's get this party staaarted
    p = text.xpath('p')

    # Name, Type, HP; Stage and Evolutionary Info
    cdata['name'] = p[0].xpath('.//span[contains(@itemprop, "title")]/text()')[0]
    tmpre = _re['th'].match(p[0].xpath('./text()[1]')[0])
    cdata['type'] = tmpre.group(1)
    cdata['hp'] = tmpre.group(2)
    stageinfo = p[0].xpath('./text()[2]')[0]
    if " – " in stageinfo:
        tmpre = _re['sef'].match(p[0].xpath('./text()[2]')[0])
        cdata['stage'] = tmpre.group(1)
        cdata['evolves_from_txt'] = tmpre.group(2)
        cdata['evolves_from'] = p[0].xpath('./a/text()')[0]
    else:
        cdata['stage'] = stageinfo

    # Flavor
    cdata['flavor'] = p[-1].xpath('./em/text()')[0]

    # Illustrator & Set Information
    ir = p[-2].xpath('./text()')
    cdata['art_credit'] = p[-2].xpath('./span[1]/a/span/text()')[0]
    cdata['art_credit_txt'] = ir[0]
    sdata['set_name'] = p[-2].xpath('./span[2]/a/span/text()')[0]
    tmpre = _re['ri'].match(ir[1])
    cdata['card_no_text'] = "%s/%s" % (tmpre.group(1), tmpre.group(2))
    cdata['card_rarity'] = tmpre.group(3)
    sdata['ncards'] = tmpre.group(2)

    # Weakness, Resistance and Retreat Cost
    wrr = p[-3].xpath('./text()')
    cdata['weakness'] = wrr[0]
    cdata['resistance'] = wrr[1]
    cdata['retreat'] = wrr[2]

    # Remaining paragraphs are moves/abilities, presumably:
    cdata['moves'] = []
    for m in p[1:-3]:
        _move = dict()
        _txt = m.xpath('./text()')
        if (len(_txt) == 2):
            _move['type'] = 'Power'
            _move['name'] = _txt[0]
            _move['txt'] = _txt[1]
        else:
            _move['type'] = 'std'
            _move['txt'] = _txt[0]
        pprint.pprint(_move)
        cdata['moves'].append(_move)

    jcard = {
        "meta": meta,
        "cdata": cdata,
        "sdata": sdata,
    }

    udata['bulbapedia'] = bulbapedia_link(jcard)
    udata['tat'] = tat_links(jcard)

    jcard['udata'] = udata
    return jcard



def get_cards(nexturl, baseurl):
    cards = list()
    for i in range(2):
        cdict = get_card(nexturl)
        nexturl = cdict['meta']['nexturl']
        cards.append(cdict)
    return cards

#if __name__ == '__main__':
#    jlist = get_cards(seed_url, base_url)
#    for j in jlist:
#        print(j)
