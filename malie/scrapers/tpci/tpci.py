#!/usr/bin/env python3

import argparse
from dataclasses import dataclass
from html import unescape
import json
import logging
import re

from malie.lib.Stopwatch import Stopwatch
from malie.lib.WebScraper import WebScraper
from malie.lib.DLO import DLO
from malie.lib.util import eprint, isodate
from malie.core.exp_db import ExpansionDatabase, LocalizedExpansion

# Scraper-specific classes

class TPCILocale:
    """
    TPCILocale represents the configuration specifics of a localization
    on TPCI's website.
    """
    uri = {
        'base': 'https://www.pokemon.com',
        'api': '/{locale}/api/expansions',
    }
    locales = {
        'us': {
            'slug': 'pokemon-tcg',
            'cardslug': 'pokemon-cards',
            'cards': 'cards',
            'series': 'Series',
            'date': 'Release Date',
            'lang': 'en',
            'delim': '—', # EM Dash
            'pdf': '.*/assets/cms2?/pdf/(?:trading-card-game|tcg)/checklists?/(.+?)(checklist|_web_cardlist_en|_(Card|Check)list_(Lo|HI)).pdf',
            'cms': 'cms',
        },
        'it': {
            'slug': 'gcc',
            'cardslug': 'archivio-carte',
            'cards': 'carte',
            'series': 'Serie',
            'date': 'Data di pubblicazione',
            'delim': ' - ', # Hyphen
            'cms': 'cms-it-it',
        },
        'uk': {
            'slug': 'pokemon-tcg',
            'cardslug': 'pokemon-cards',
            'cards': 'cards',
            'series': 'Series',
            'date': 'Release Date',
            'lang': 'en',
            'delim': '—', # EM Dash
            'pdf': '.*/assets/(?:cms|cms-en-uk)/pdf/tcg/checklists/(.+?)_(?:Card|Check)[lL]ist_(Lo|HI|EN).pdf',
            'cms': 'cms-en-uk',
        },
        'es': {
            'slug': 'jcc-pokemon',
            'cardslug': 'cartas-pokemon',
            'cards_slug': '{cards}-(.+)',
            'cards': 'cartas',
            'series': 'Serie',
            'date': 'Fecha de lanzamiento',
            'delim': '-', # Hyphen
            'cms': 'cms-es-es',
        },
        'fr': {
            'slug': 'jcc-pokemon',
            'cardslug': 'cartes-pokemon',
            'cards_slug': '{cards}-(.+)',
            'cards': 'cartes',
            'series': 'Série',
            'date': 'Date de sortie',
            'delim': ' – ', # EN dash
            'cms': 'cms-fr-fr',
        },
        'de': {
            'slug': 'pokemon-sammelkartenspiel',
            'cardslug': 'pokemon-karten',
            'cards': 'karten',
            'series': 'Serien',
            'date': 'Erscheinungsdatum',
            'delim': ' – ', # EN dash
            'cms': 'cms-de-de',
        },
    }

    @staticmethod
    def base_url():
        return TPCILocale.uri['base']

    def api_url(self):
        return self.base_url() + (TPCILocale.uri['api'].format(locale=self._locale_str))

    def name(self):
        return self._locale_str

    def lang(self):
        return self._locale.get('lang', self.name())

    def cards_slug(self):
        expr = self._locale.get('cards_slug', "(.+)-{cards}")
        return expr.format(cards=self._locale['cards'])

    def cards_base_url(self):
        return "{}/{}/{}".format(self.name(), self['slug'], self['cardslug'])

    def __getitem__(self, key):
        return self._locale[key]

    def __contains__(self, key):
        answer = key in self._locale
        return answer

    def __init__(self, locale_str):
        self._locale_str = locale_str
        self._locale = TPCILocale.locales[locale_str]


@dataclass
class TPCIRaw:
    '''Raw expansion data from TPCI, with
    settings and locale from the ExpScraper.'''
    locale: TPCILocale
    data: dict


class TPCIIterator:
    '''TPCIIterator is an iterator that allows one to iterate over TPCI's
    expansion JSON API, reading out raw set data one at a time.'''
    def __init__(self, locale=None, web=None):
        self.locale = locale or TPCILocale('us')
        self.web = web or WebScraper()

        self._exps = []
        self._num = 0
        self._index = 0
        self._batch = 12
        self._limit = 0

    def fetch(self):
        n = self._num
        batch = self._batch
        if self._limit and (self._limit - n) < batch:
            batch = self._limit - n
        if not batch:
            return False
        url = self.locale.api_url() + '?index={index:d}&count={count:d}'.format(
            index=self._index,
            count=batch)
        obj = self.web.fetch(url)
        j = json.loads(obj.content)
        if j:
            self._exps.extend(j)
            self._index += batch
            self._num += batch
            return True
        return False

    def __iter__(self):
        return self

    def __next__(self):
        if not (self._exps or self.fetch()):
            raise StopIteration
        return TPCIRaw(self.locale, self._exps.pop(0))

class InsufficientData(Exception):
    pass

class TPCIExpansion:
    def __init__(self, web: WebScraper = None):
        self.data = DLO()
        self.locale = None
        self.web = web or WebScraper()

    @classmethod
    def from_tpciraw(cls, raw: TPCIRaw, web: WebScraper = None):
        tpci_exp = cls(web)
        tpci_exp.add_raw(raw)
        return tpci_exp

    def add_original_title(self, original_title):
        self.data['title-original'] = original_title
        # Some sets (HGSS era, US) have an extraneous prefix;
        self.data['title'] = original_title.split('Pokémon TCG: ')[-1]
        self.data['title-common'] = self.data['title'].split(self.locale['delim'])[-1]
        self.data['title-short'] = self.data['title-common'].split('EX ')[-1]

    def add_logo_thumbnail(self, thumbnail):
        self.data['thumbnail'] = thumbnail
        res = re.match(r".*/series/(.+?)/(.+?)/.*\.[jpe?g|gif|png]",
                       thumbnail)
        self.data['tn-series-slug'] = res.group(1)
        self.data['thumbnail-slug'] = res.group(2)

    def add_release_date(self, release_date):
        self.data['releaseDate'] = isodate(release_date)

    def add_raw(self, raw: TPCIRaw):
        exp = raw.data
        self.locale = raw.locale
        for k, v in exp.items():
            exp[k] = unescape(v) if isinstance(v, str) else v

        self.add_original_title(exp.pop('title'))
        self.data['url'] = self.locale.base_url() + exp.pop('url')
        self.data['series'] = exp.pop('system')
        # ID is useless because it's a transaction ID, not a persistent one.
        del exp['id']
        # Convert date from localized string to an ISO date
        self.add_release_date(exp.pop('releaseDate'))
        self.add_logo_thumbnail(exp.pop('thumbnail'))
        # Copy remaining items to DLO (slug, others?)
        self.data.update(exp)

    def add_theme_deck_url(self, url):
        if not url:
            return
        res = re.match(r"/{}/{}/(.+?)/".format(self.locale.name(), self.locale['slug']), url)
        self.data['theme-deck-url'] = self.locale.base_url() + url
        self.data['slug'] = res.group(1)

    def add_featured_url(self, url):
        if not url:
            return
        ftsslug, ftfslug = self._parse_feat_slugs(url)
        self.data['slug'] = ftsslug
        self.data['slug'] = ftfslug
        self.data['featured-cards-url'] = self.locale.base_url() + url

    def add_checklist_url(self, url):
        if not url:
            return
        token = self._parse_pdf_token(url)
        self.data['checklist-url'] = url
        self.data['pdf-slug'] = token

    def add_database_url(self, url):
        if not url:
            return
        res = re.match(r".*/\?(.+?)=on.*", url)
        self.data['database-url'] = self.locale.base_url() + url
        self.data['cdb-id'] = res.group(1)

    def add_setpage_icon(self, url):
        if not url:
            return
        res = re.match(r".*/([^\/]+?)(_|[-_]expansion[-_])symbol([-_]38(x38)?)?.(png|jpg)",
                       url)
        self.data['icon'] = url
        if not res:
            print("No match for icon-slug in URL")
            print(url)
        self.data['icon-slug'] = res.group(1)

    def process_tpci_url(self):
        if 'url' not in self.data:
            raise InsufficientData("process_tpci_url requires 'url' field")

        url = self.data['url']
        tree = self.web.fetch_tree(url)

        def _link_url(cname):
            li_elem = tree.find_class1(cname)
            if li_elem is not None:
                return li_elem.xpath('a')[0].attrib['href']
            return None

        self.add_theme_deck_url(_link_url('theme-decks'))
        self.add_featured_url(_link_url('featured'))
        self.add_checklist_url(_link_url('checklist'))
        self.add_database_url(_link_url('database'))

        self.add_original_title(tree.xpath('//article/h1')[0].text)
        self.add_setpage_icon(tree.xpath('//article/div/img')[0].attrib['src'])

        path = './/td[text()="{token}"]/following-sibling::td'
        series_path = path.format(token=self.locale['series'])
        self.data['series'] = tree.xpath(series_path)[0].text
        date_path = path.format(token=self.locale['date'])
        self.add_release_date(tree.xpath(date_path)[0].text)

    # set-page parsing and helpers:

    def _parse_feat_slugs(self, url):
        if url:
            expr = "/%s/%s/(.+?)/%s" % (self.locale.name(), self.locale['slug'],
                                        self.locale.cards_slug())
            res = re.match(expr, url)
            return (res.group(1), res.group(2))
        return None, None

    def _parse_pdf_token(self, url):
        if not url:
            return None
        expr = '.*/assets/cms2-%s-%s/pdf/trading-card-game/checklist/(.+?)_web_cardlist_%s.pdf' % (
            self.locale.lang(),
            self.locale.name(),
            self.locale.lang())
        res = re.match(expr, url)
        if res:
            return res.group(1)
        if 'pdf' in self.locale:
            res = re.match(self.locale['pdf'], url)
        if res:
            return res.group(1)
        raise Exception("Parsing PDF token failed: url: '%s'" % url)

    def add_sample_card(self, url):
        expr = r"/{}/(.+?)/(.+?)/.+?/".format(self.locale.cards_base_url())
        res = re.match(expr, url)
        self.data.overwrite('sample-card-url', url)
        self.data['series-slug'] = res.group(1)
        self.data['card-slug'] = res.group(2)

    # requires any of {cdb-id, database-url}
    # produces {series-slug, card-slug, sample-card-url}
    def process_database_page(self):
        if not {'database-url', 'cdb-id'} ^ self.data.keys():
            raise InsufficientData("process_database_page requires 'database-url' or 'cdb-id' fields")

        if 'database-url' not in self.data:
            assert 'cdb-id' in self.data
            url = "/{}/?{}=on".format(
                self.locale.cards_base_url(),
                self.data['cdb-id'])
            self.add_database_url(url)
        # cdb-id must be set; because add_database_url sets it.
        assert 'cdb-id' in self.data

        tree = self.web.fetch_tree(self.data['database-url'])
        cardurl = tree.find_class1('cards-grid').xpath('li[1]/a')[0].attrib['href']
        self.add_sample_card(cardurl)

    def process_feature_page(self):
        if 'featured-cards-url' not in self.data:
            raise InsufficientData("process_feature_page requires 'featured-cards-url' field")
        tree = self.web.fetch_tree(self.data['featured-cards-url'])
        tmp = tree.find_class1('expansion-cards')
        cardurl = tmp.xpath('li[1]/div/div/a')[0].attrib['href']
        self.add_sample_card(cardurl)

    def scrape_card_database_slugs(self):
        if 'database-url' in self.data:
            self.process_database_page()
        elif 'featured-cards-url' in self.data:
            self.process_feature_page()
        else:
            eprint("WARNING: No featured cards page or database link for expansion {}.".format(
                self.data['title']))

    def determine_id(self):
        if 'card-slug' in self.data:
            self.data['id'] = self.data['card-slug']
        else:
            # This page is probably Generations... and the icon slug happens to be correct.
            self.data['id'] = self.data['icon-slug']

    def guess_outlinks(self):
        '''Try to guess outlinks.'''
        if self.locale.name() != 'us':
            return
        out = DLO()

        # Bulbapedia (uses title-common)
        bulbapedia = 'https://bulbapedia.bulbagarden.net/wiki/%s_(TCG)' % (
            self.data['title-common'].replace(' ', '_'))
        eprint("guessing bulbapedia: '%s'" % bulbapedia)
        if self.web.check_url(bulbapedia):
            out['bulbapedia'] = bulbapedia

        # pkmncards (uses title-short)
        pkmncards = 'https://pkmncards.com/set/%s/' % (
            self.data['title-short'].lower().replace(' ', '-'))
        eprint("guessing pkmncards: '%s'" % pkmncards)
        if self.web.check_url(pkmncards):
            out['pkmncards'] = pkmncards

        if out:
            self.data['outlinks'] = out

    def scrape(self):
        '''Oh, Baby!'''
        self.process_tpci_url()
        self.scrape_card_database_slugs()
        self.guess_outlinks()
        self.determine_id()


class TPCIDatabaseScraper:
    def __init__(self, database, webscraper=None, lazy=True):
        self._database = database
        self._web = webscraper or WebScraper()
        self.lazy = lazy

    def scrape_db_pages(self, locales=None):
        if not locales:
            locales = TPCILocale.locales.keys()

        for locname in locales:
            locale = TPCILocale(locname)
            tree = self._web.fetch_tree("{}/{}".format(
                locale.base_url(),
                locale.cards_base_url()))
            node = tree.xpath('//*[@id="filterExpansions"]')[0]
            for series in node.xpath('./div/fieldset'):
                self.scrape_series(locale, series)

    def scrape_series(self, locale, series):
        series_name = series.xpath('./div/div/div/h2/text()')[0]
        logging.debug("DB page series - %s", series_name)
        for expansion in series.xpath('./div/div/ul/li'):
            self.parse_expansion(locale, expansion)

    def parse_expansion(self, locale, expansion):
        exp = TPCIExpansion(self._web)
        exp.locale = locale
        data = exp.data

        # det-sm, 9-sm, etc
        data['cdb-id'] = expansion.xpath('./input')[0].attrib['id']
        data['cdb-id'] = expansion.xpath('./input')[0].attrib['name']
        data['cdb-id'] = expansion.xpath('./label')[0].attrib['for']
        # "Detective Pikachu", etc
        data['title-cdb'] = expansion.xpath('./label/span')[0].text.split('—')[-1]

        links = expansion.xpath('./label/i')[0].iterlinks()
        # Each link is (element, attribute, link, pos)
        data['cdb-icon'] = list(links)[0][2]
        regex = "".join((r"https://assets.pokemon.com/assets/",
                         "{}/".format(locale['cms']),
                         r"img/tcg/expansion-symbols/",
                         r"(.+?)(?:-expansion-symbol|-exp-symbol|symbol)?.png"))
        match = re.match(regex, data['cdb-icon'])
        data['cdb-icon-id'] = match[1]

        search = (self._database.find_set_by_cdbid(data['cdb-id']) or
                  self._database.find_set_by_name(data['title-cdb'], [locale.name()]))
        miss = {}
        if search:
            logging.debug("searching for match of cdb-id: %s; found set %s",
                          data['cdb-id'], search)
            ldb = LocalizedExpansion(locale.name(), self._database[search])
            keys = {'sample-card-url', 'series-slug', 'card-slug'}
            miss = keys - ldb.keys()
            data['id'] = ldb['id']

        if (not self.lazy) or (not search) or (miss):
            exp.process_database_page()
            exp.determine_id()

        if not search:
            logging.info("new set found: %s", data['id'])
        elif not ldb.localized():
            logging.info("new set found for locale %s: %s", locale.name(), data['id'])
        elif miss:
            logging.info("found new info for locale %s: %s", locale.name(), data['id'])
        self._database.update(exp)
        self._database.write()


def scrape(database, web, locales, watch=False, num=None, delay=5):
    s = Stopwatch()

    for locname in locales:
        loc = TPCILocale(locname)
        stale_counter = 0
        for raw in TPCIIterator(loc, web):
            exp = TPCIExpansion.from_tpciraw(raw, web)
            exp.scrape()
            if watch:
                eprint(json.dumps(exp.data, indent=4))
            if not database.update(exp):
                stale_counter += 1
                logging.info("Nothing new for %s:%s ...", locname, exp.data['id'])
            else:
                stale_counter = 0
            if num and stale_counter >= num:
                logging.info("Seemingly nothing new in loc:%s; skipping ahead...", locname)
                break
            if database.fname:
                database.write()
            s.snooze(delay)


def main():
    parser = argparse.ArgumentParser(description='TPCI Expansion Scraper',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-r', dest='retries', required=False,
                        type=int, default=5, action='store',
                        help="number of times to retry a fetch operation")
    parser.add_argument('-D', dest='errorDelay', required=False,
                        type=int, default=30, action='store',
                        help="Delay before retrying a failed fetch")
    parser.add_argument('-v', dest='verbose', required=False, action='store_true',
                        help="print verbose messages to stderr")
    parser.add_argument('-V', dest='debug', required=False, action='store_true',
                        help="print debug messages")
    parser.add_argument('-W', dest='watch', required=False, action='store_true',
                        help="Watch scraped expansions on stderr")
    parser.add_argument('-o', dest='outputFile', required=False, action='store')
    parser.add_argument('-d', dest='delay', required=False,
                        type=int, default=5, action='store',
                        help="Delay in seconds between scraping each expansion.")
    parser.add_argument('-p', dest='print', required=False, action='store_true',
                        help="Pipe JSON database to stdout on conclusion.")
    parser.add_argument('-n', dest='num', required=False,
                        type=int, default=3, action='store',
                        help="Number of 'stale' sets to scrape before trying the next locale."
                        " 0 forces a full refresh.")
    parser.add_argument('-l', dest='locale', required=False, action='store', default=None,
                        help="Scrape just the specified locale, instead of all available locales.")
    parser.add_argument('-s', dest='search', required=False, action='store_true',
                        help="Scrape the TPCi search page")
    args = parser.parse_args()

    database = ExpansionDatabase(args.outputFile)
    web = WebScraper(retries=args.retries, delay=args.errorDelay)

    if args.locale:
        locales = (args.locale, )
    else:
        locales = TPCILocale.locales.keys()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)

    scrape(database, web, locales, watch=args.watch, num=args.num, delay=args.delay)

    if args.search:
        lazy = args.num != 0
        scraper = TPCIDatabaseScraper(database, web, lazy=lazy)
        scraper.scrape_db_pages(locales)

    if args.print:
        print(json.dumps(database.data, indent=2))


if __name__ == '__main__':
    main()
