import re
import xmltodict

def keymapfn(key):
    return re.sub('_x([0-9,a-f,A-F]{4})_',
                  lambda match: chr(int(match[1], base=16)), key)

def valmap(key, value):
    if key in {'HP', 'Retreat'}:
        return int(value)
    if value is None:
        return ""
    return value

def rowmap(row):
    res = {}
    for key, value in row.items():
        key = keymapfn(key)
        res[key] = valmap(key, value)
    return res

def parse(strdata: str) -> dict:
    return xmltodict.parse(strdata)

def standardize(data: dict) -> dict:
    """Take a v1.0+ database as returned from parse and turn it into a common format."""

    if 'NewDataSet' in data:
        # v1.0
        root = 'NewDataSet'
    else:
        # v1.3 -- errant XML for GUM_EN
        root = 'DocumentElement'

    if len(data[root]) != 1:
        raise NotImplementedError("More than one data set found, this format is unsupported")

    for key, value in data[root].items():
        return {
            'name': key,
            'rows': [rowmap(row) for row in value]
        }
