import logging
import struct

class ParseError(RuntimeError):
    pass

def read_length(f):
    # Inspired by MS-NRBF 2.1.1.6 Length-Prefixed String
    length = 0
    shift = 0
    for i in range(5):
        data = f.read(1)[0]
        length += (data & ~0x80) << shift
        shift += 7
        if not data & 0x80:
            break
        elif i == 5:
            raise Exception("Variable Length (%d) exceeds maximum size" % i)
    return length

def read_string(f):
    n = read_length(f)
    return f.read(n)

def read_field(f):
    name = read_string(f)
    ftype = read_string(f)
    return (name.decode(), ftype.decode())

def get_schema(f):
    # Note: this relies on insertion order dicts in python3.7+.
    num_fields = struct.unpack('<I', f.read(4))[0]
    schema = {}
    for _ in range(num_fields):
        fieldname, fieldtype = read_field(f)
        schema[fieldname] = fieldtype
    return schema

def read_row(f, schema):
    record = {}
    for field, ftype in schema.items():
        entry = f.read(1)[0]
        if entry == 0x00:
            # Appears to mean "Entry is present."
            if ftype == 'System.String':
                value = read_string(f).decode()
            elif ftype == 'System.Int32':
                value = struct.unpack('<I', f.read(4))[0]
            else:
                raise Exception("Unknown type")
            logging.debug("%s := '%s'", field, str(value))
            record[field] = value
        elif entry == 0x01:
            # Appears to mean that there is no entry.
            pass
        else:
            raise Exception(f"unknown entry marker {entry:d}")
    return record

def parse_stream(handle):
    if handle.read(1) != b'\x00':
        raise ParseError("Unexpected database format; first byte was not 0x00")

    name = read_string(handle).decode()
    logging.debug("database: %s", name)
    schema = get_schema(handle)
    num_rows = struct.unpack('<I', handle.read(4))[0]

    return {
        'name': name,
        'schema': schema,
        'rows': [read_row(handle, schema) for _ in range(num_rows)]
    }

def parse_file(filename):
    with open(filename, "rb") as handle:
        return parse_stream(handle)

def standardize(data: dict) -> dict:
    """
    Take a v1.2+ database as returned by parse_<stream|file> and turn it into a common format.
    """
    return {
        'name': data['name'],
        'rows': data['rows']
    }
