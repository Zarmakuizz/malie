#!/usr/bin/env python3

import logging
import os
import subprocess
import tempfile

import PIL.Image

from malie.scrapers.ptcgo import logger

# Magic numbers obtained from card mesh
RATIO = (63 / 88)
RADIUS = (7.952214423447161/255.96)

LOGGER = logger.get_logger()


def mkdir(filename):
    dirname = os.path.dirname(filename)
    if dirname:
        os.makedirs(dirname, exist_ok=True)


def crop(infile, outfile):
    img = PIL.Image.open(infile)
    ideal = (RATIO * img.height)

    # This might round in either direction, but half_width will be even.
    half_width = round(ideal / 2)
    full_width = half_width * 2

    margin = (img.width - full_width) // 2
    assert (img.width - (2 * margin)) == full_width

    radius_px = round(RADIUS * img.height)

    args = [
        "convert",
        infile,
        '-strip',
        '-crop', f"{full_width:d}x{img.height:d}+{margin:d}+0",
        '(',
        '+clone',
        '-alpha', 'extract',
        '-draw',
        f"fill black polygon 0,0 0,{radius_px} {radius_px},0 fill white circle {radius_px},{radius_px} {radius_px},0",
        '(', '+clone', '-flip', ')',
        '-compose', 'Multiply',
        '-composite',
        '(', '+clone', '-flop', ')',
        '-compose', 'Multiply',
        '-composite',
        ')',
        '-alpha', 'off',
        '-compose', 'CopyOpacity',
        '-composite',
        outfile
    ]

    mkdir(outfile)

    LOGGER.debug(" ".join(args))
    subprocess.run(args, check=True)
    LOGGER.info("crop %s OK", infile)


def crush(infile, outfile, paranoid=False):
    mkdir(outfile)

    level = 6 if paranoid else 4
    env = dict(os.environ)
    env.setdefault('PATH', os.defpath)
    env['PATH'] += os.pathsep + os.path.expanduser('~/.cargo/bin')
    args = ['oxipng', '-o', str(level),
            '--strip', 'all', infile, '--out', outfile]
    LOGGER.debug(" ".join(args))
    subprocess.run(args, env=env, check=True)
    LOGGER.info("crush %s OK", outfile)


def jpg(infile, outfile):
    mkdir(outfile)

    img = PIL.Image.open(infile)
    out = PIL.Image.new('RGB', img.size, 0xFFFFFF)
    out.paste(img, (0, 0), img)
    out.save(outfile, format='jpeg', quality=85)
    LOGGER.info("jpg %s OK", outfile)


def crop_crush(infile, outfile):
    with tempfile.NamedTemporaryFile() as tmp:
        crop(infile, tmp.name)
        crush(tmp.name, outfile)


def crop_jpg(infile, outfile):
    with tempfile.NamedTemporaryFile() as tmp:
        crop(infile, tmp.name)
        jpg(tmp.name, outfile)


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="File to crop")
    parser.add_argument("outfile", help="Output file location")
    parser.add_argument('--crush', dest='crush', required=False, action="store_true")
    parser.add_argument('--jpg', dest='jpg', required=False, action="store_true")
    parser.add_argument('--debug', required=False, action="store_true")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if args.crush and args.jpg:
        print("--crush and --jpg arguments are mutually exclusive.")
        return

    if args.crush:
        crop_crush(args.infile, args.outfile)
    elif args.jpg:
        crop_jpg(args.infile, args.outfile)
    else:
        crop(args.infile, args.outfile)


if __name__ == '__main__':
    main()
