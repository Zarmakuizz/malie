#!/usr/bin/python3

import re


class StringTable:
    def __init__(self, encoded_bytes):
        self._raw = self._decode(encoded_bytes)
        self._str = self._raw.decode()
        self._indices = {}

    @staticmethod
    def _decode(encoded_bytes):
        return bytes([((i ^ b ^ 170) & 0xff) for
                      i, b in enumerate(encoded_bytes)])

    def __getitem__(self, key):
        return self._indices[key]

    def __iter__(self):
        return iter(self._indices)

    def values(self):
        return self._indices.values()

    def items(self):
        return self._indices.items()

    def string(self, index, length):
        return self._raw[index:(index + length)].decode()

    def stringi(self, index, length):
        return self._str[index:(index + length)]

    def add_index(self, index, offset, length):
        self._indices[index] = self.string(offset, length)

    def find(self, needle):
        for match in self.search(needle):
            print("Match: [{} - {})".format(match.start(), match.end()))
            print("  {}{}{}".format(
                self.stringi(match.start() - 50, 50),
                self._str[match.start() : match.end()],
                self.stringi(match.end(), 50)))

    def search(self, needle):
        return re.finditer(needle, self._str)
