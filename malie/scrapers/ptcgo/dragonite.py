import atexit
import json

from b2sdk.account_info.sqlite_account_info import (SqliteAccountInfo)
from b2sdk.api import (B2Api)
from b2sdk.cache import (AuthInfoCache)

from malie.lib.cache import Cache


def cache():
    return Cache('dragonite')


class Dragonite:
    def __init__(self, configuration):
        self.info = SqliteAccountInfo()
        self.api = B2Api(self.info, AuthInfoCache(self.info))
        self.bucket = self.api.get_bucket_by_name(configuration.get('b2-bucket'))
        self.cache = cache()
        self._files = {}
        self.load()
        atexit.register(self.flush)

    def load(self):
        self._files = {}
        try:
            data = self.cache.open('files.json')
            entries = json.load(data)
            for entry in entries:
                self._files[entry['fileName']] = entry
        except FileNotFoundError:
            self.refresh()

    def _process_entry(self, response):
        entry = response.as_dict()
        entry['sha1'] = response.content_sha1
        entry['content_type'] = response.content_type
        self._files[entry['fileName']] = entry
        return entry

    def refresh(self):
        self._files = {}
        rsp = self.bucket.ls(recursive=True)
        for item in rsp:
            self._process_entry(item[0])
        self.flush()

    def flush(self):
        tmp = self._files.values()
        self.cache.put("files.json",
                       fetcher=lambda: json.dumps(list(tmp)).encode(),
                       force=True)

    def upload(self, path: str, remote: str):
        if remote in self._files:
            return {}
        rsp = self.bucket.upload_local_file(path, remote)
        return self._process_entry(rsp)

    def exists(self, path: str):
        return path in self._files
