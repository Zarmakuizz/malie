#!/usr/bin/python3
"""
unown provides functionality to store and retrieve I18N translation strings.

unown provides three main classes: UnownZukan, Unown, and UnownSlice.
Together, they provide a store for key:string lookups for Pokemon TCG Online
internationalization strings.

UnownZukan is a mapping of locales to Unown objects, with serialization and
           deserialization methods.
UnownSlice provides the base functionality for the mappings themselves within
           a single locale.
Unown inherits from UnownSlice to provide additional serialization options.
"""

import json
import re
import os

from malie.scrapers.ptcgo import logger

LOGGER = logger.get_logger()


class UnownSlice:
    """
    UnownSlice represents I18N key to string mappings for a single locale.

    It differs from the Unown class in that it is meant to portray search
    results or filtered selections from the primary class, and offers no
    methods to add, load, or save the data.
    """

    def __init__(self, locale, entries):
        self.database = entries
        self.locale = locale

    def __str__(self):
        return str(self.database)

    def __repr__(self):
        return "%s(%s, %s)" % (self.__class__.__name__,
                               self.locale,
                               self.__str__())

    def lookup(self, key, default=None, strict=False):
        """
        Look up an I18N key and return the localized string, or a default.

        The default defaults to None, but can be set to e.g. the empty string.
        """
        key = key.lower()
        if key in self.database or strict:
            return self.database[key]
        return default

    def key_search(self, needle):
        """
        Return an UnownSlice with only I18N keys that are supersets of `needle`.

        For a given needle, return a UnownSlice containing only key:value pairs
        where needle is a substring of each key.
        """
        match = {k: v for k, v in self.database.items() if needle.lower() in k.lower()}
        return UnownSlice(self.locale, match)

    def search(self, needle):
        """
        Return an UnownSlice with only I18N strings that are supersets of `needle`.

        For a given needle, return a UnownSlice containing only key:value pairs
        where needle is a substring of each value.
        """
        match = {k: v for k, v in self.database.items() if needle.lower() in v.lower()}
        return UnownSlice(self.locale, match)

    def key_filter(self, needle):
        """
        Return an UnownSlice with only I18N keys that are not supersets of `needle`.

        For a given needle, return a UnownSlice containing only key:value pairs
        where needle is not a substring of each key.
        """
        match = {k: v for k, v in self.database.items() if not needle.lower() in k.lower()}
        return UnownSlice(self.locale, match)

    def filter(self, needle):
        """
        Return an UnownSlice with only I18N strings that are not supersets of `needle`.

        For a given needle, return a UnownSlice containing only key:value pairs
        where needle is not a substring of each value.
        """
        match = {k: v for k, v in self.database.items() if not needle.lower() in v.lower()}
        return UnownSlice(self.locale, match)

    def detokenize(self, string, default=None, strict=False):
        """Given a string, replace any I18N tokens with its localized string."""
        def _default_fn(match):
            if default is not None:
                return default
            return "{}{}{}".format(match[1], match[2], match[3])

        def _replace_fn(match):
            return self.lookup(match[2], _default_fn(match), strict)

        return re.sub(r"(\$\$?\$?)(.*?)(\$\$?\$?)", _replace_fn, string)


class Unown(UnownSlice):
    """
    Unown represents mutable I18N key to string mappings for a single locale.

    Unown features extended functionaly over UnownSlice for adding, loading,
    and saving entries.
    """

    def __init__(self, locale):
        self.releases = {}
        self.version = None
        super().__init__(locale, {})

    def add_release(self, release, checksum, database):
        """
        Adds a collection of localization strings (a "release") to the database.

        release: The name of the release (i.e. "CRSM11")
        checksum: A checksum provided identifying the release version
        database: a dict of key:value pairs.
        """
        reldata = self.releases.setdefault(release, {})
        reldata['checksum'] = checksum
        entries = reldata.setdefault('database', {})

        for key, val in database.items():
            self.database[key] = val
            entries[key] = val

    def _add(self, key: str, release: dict) -> None:
        """
        Adds a collection of localization strings (a "release") to the database.

        key: The name of the release (i.e. "CRSM11")
        release: The release object itself. It's a dict that looks like this:

        release = {
            'md5': 'fafb7bb71b523d78ff4339d9af60133a',
            'localizationList': [
                { 'key': 'localization.key',
                  'value': 'localization value' }
            ]
        }
        """
        database = {ent['key']: ent['value'] for ent in release['localizationList']}
        self.add_release(key, release['md5'], database)

    def add(self, releases, version):
        """For a dict mapping release names to releases, add each release to the database."""
        for k in releases:
            self._add(k, releases[k])
        self.version = version

    def release_names(self):
        return tuple(self.releases.keys())

    def release_dict(self):
        return {release: reldata['checksum'] for release, reldata in self.releases.items()}

    def dump_release(self, release):
        reldata = self.releases[release]
        return {
            'locale': self.locale,
            'release': release,
            'checksum': reldata['checksum'],
            'database': reldata['database']
        }

    def load_release(self, data):
        """Load a release (e.g. from file), overwriting entirely the existing release."""
        assert self.locale == data['locale']
        self.releases[data['release']] = {}
        self.add_release(data['release'],
                         data['checksum'],
                         data['database'])


class UnownZukan:
    """UnownZukan represents a collection of Unown dictionaries for multiple locales."""

    def __init__(self, filepath=None):
        self.zukan = {}
        self.filepath = filepath
        self.empty = Unown('')
        if filepath:
            try:
                self.load(filepath)
            except FileNotFoundError:
                pass

    def __getitem__(self, key):
        return self.zukan[key]

    def get(self, key):
        if key not in self.zukan:
            self.zukan[key] = Unown(key)
        return self.zukan[key]

    def add(self, obj):
        """Add releases from an AllLocalizationReleases object to the Zukan."""
        unown = self.get(obj['locale'])
        unown.add(obj['releases'], version=obj['version'])

    def save(self, filepath=None, dirname='releases'):
        """
        Serializes the Zukan to a json file.

        The V3 format saves each release to a different JSON file,
        which allows for efficient updating of outdated parts.

        It saves in this format:
        {
          $locale: {
            "version": $version,
            "releases": {
              $release: {
                "checksum": $checksum,
                "database": $path
              }
            }
          }
        }

        where $path is the location of another JSON file that contains the actual translation data.
        The format of that translation data is:

        {
          "locale": "en_US",
          "release": "CR140",
          "checksum": "7bb68d0acb21cff663c3b2a9edcb6289",
          "database": {
            $key: $value
          }
        }

        The pathname generated for the individual translation files is not ABI, but will be:
        {locale}-{release}-{checksum}.json
        """
        self.filepath = filepath or self.filepath
        dname = os.path.dirname(self.filepath)
        subdir = os.path.join(dname, dirname)
        os.makedirs(subdir, exist_ok=True)

        directory = {}
        for locale, locale_data in self.zukan.items():
            data = directory.setdefault(locale, {})
            data['version'] = locale_data.version
            data['releases'] = {}

            for release, reldata in locale_data.releases.items():
                reldata_out = data['releases'].setdefault(release, {})
                checksum = reldata['checksum']
                reldata_out['checksum'] = checksum

                relfile = f"{locale}-{release}-{checksum}.json"
                relpath = os.path.join(dirname, relfile)
                abspath = os.path.join(subdir, relfile)

                with open(abspath, "w") as outfile:
                    json.dump(locale_data.dump_release(release), outfile)
                reldata_out['database'] = relpath

        with open(self.filepath, "w") as outfile:
            json.dump(directory, outfile)

    def load(self, filepath):
        """Loads the Zukan from file."""

        LOGGER.debug("Loading %s", filepath)
        with open(filepath, 'rb') as infile:
            self.filepath = filepath
            obj = json.load(infile)
        prefix = os.path.dirname(self.filepath)

        for locale, data in obj.items():
            LOGGER.debug("Loading localization data for locale:%s", locale)
            unown = self.get(locale)
            unown.version = data['version']
            for release, reldata in data['releases'].items():
                subpath = os.path.join(prefix, reldata['database'])
                LOGGER.debug("Loading release data from release file %s", subpath)
                with open(subpath, "r") as infile:
                    rawdata = json.load(infile)
                    assert rawdata['locale'] == locale
                    assert rawdata['release'] == release
                    assert rawdata['checksum'] == reldata['checksum']
                    unown.load_release(rawdata)

    def locales(self):
        """Return a tuple of all locales present in the Zukan."""
        return tuple(self.zukan.keys())

    def releases(self, locale):
        """Return a dict of {release: checksum} entries present for a given locale."""
        if locale in self.zukan:
            return self.zukan[locale].release_dict()
        return {}

    def lookup(self, key, locale=None):
        """
        Return localized strings matching a specified I18N key.

        If locale is specified, return only that locale's string.
        Otherwise, return a dict of `locale: string` mappings.
        """
        if locale:
            return self.zukan[locale].lookup(key)
        return {k: self.zukan[k].lookup(key) for k in self.zukan}

    def detokenize(self, locale, string, default=None, strict=False):
        """
        Replace any I18N tokens in string with their localized equivalent.
        """
        if (locale not in self.zukan) and not strict:
            return self.empty.detokenize(string, default, strict)
        return self.zukan[locale].detokenize(string, default, strict)
