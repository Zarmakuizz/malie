#!/usr/bin/python3
import re
import requests
from lxml import html

def product(li_node):
    title = li_node.xpath('div/a/img')[0].attrib['alt'].strip()
    assert(title == li_node.xpath('div/div/div[1]')[0].text.strip())
    short = re.search('「(.*?)」', title)[1]
    url = li_node.xpath('div/div/div[3]/a')[0].attrib['href']
    matches = re.search(r"/products/(.+?)/(.+?)\.html", url)
    if matches:
        series_id = matches[1]
        prod_id = matches[2]
    else:
        matches = re.search(r"/ex/(.+?)/", url)
        series_id = None
        prod_id = matches[1]
    js = {
        'prod_id': prod_id,
        'img': li_node.xpath('div/a')[0].attrib['href'],
	'tn': li_node.xpath('div/a/img')[0].attrib['src'],
        'title': title,
        'shortname': short,
        'reldate': li_node.xpath('div/div/ul/li[1]/span[2]')[0].text,
        'price': li_node.xpath('div/div/ul/li[2]/span[2]')[0].text,
        'contents': li_node.xpath('div/div/ul/li[3]/span[2]')[0].text,
        'url': li_node.xpath('div/div/div[3]/a')[0].attrib['href']
    }
    if series_id:
        js['series_id'] = series_id
    return js

r = requests.get("https://www.pokemon-card.com/products/")
page = html.fromstring(r.content)
expansions = []

for li_node in page.xpath('//*[@id="tab_type1_cont0"]/ul/li'):
    js = product(li_node)
    print("%s\t%s" % (js['prod_id'], js['shortname']))
    expansions.append(product(li_node))
