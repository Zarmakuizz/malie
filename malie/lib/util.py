#!/usr/bin/env python3
"""Miscellaneous utilities to be shared by other modules."""

import re
from sys import stderr as sys_stderr
import time
import unicodedata

import dateparser

def eprint(*args, **kwargs):
    '''stackoverflow: #5574702'''
    print(*args, file=sys_stderr, **kwargs)

def isodate(stringdate):
    return dateparser.parse(stringdate).date().isoformat()

LATIN_TRANSLATE = str.maketrans({
    ' ': ' ',
    'ª': 'a',
    'º': 'o',
    '¹': '1',
    '²': '2',
    '³': '3',
    '¼': '1/4',
    '½': '1/2',
    '¾': '3/4',
    'À': 'A',
    'Á': 'A',
    'Â': 'A',
    'Ã': 'A',
    'Ä': 'A',
    'Å': 'A',
    'Æ': 'AE',
    'Ç': 'C',
    'È': 'E',
    'É': 'E',
    'Ê': 'E',
    'Ë': 'E',
    'Ì': 'I',
    'Í': 'I',
    'Î': 'I',
    'Ï': 'I',
    'Ð': 'TH',
    'Ñ': 'N',
    'Ò': 'O',
    'Ó': 'O',
    'Ô': 'O',
    'Õ': 'O',
    'Ö': 'O',
    'Ø': 'OE',
    'Ù': 'U',
    'Ú': 'U',
    'Û': 'U',
    'Ü': 'U',
    'Ý': 'Y',
    'Þ': 'TH',
    'ß': 'SS',
    'à': 'a',
    'á': 'a',
    'â': 'a',
    'ã': 'a',
    'ä': 'a',
    'å': 'a',
    'æ': 'ae',
    'ç': 'c',
    'è': 'e',
    'é': 'e',
    'ê': 'e',
    'ë': 'e',
    'ì': 'i',
    'í': 'i',
    'î': 'i',
    'ï': 'i',
    'ð': 'th',
    'ñ': 'n',
    'ò': 'o',
    'ó': 'o',
    'ô': 'o',
    'õ': 'o',
    'ö': 'o',
    'ù': 'u',
    'ú': 'u',
    'û': 'u',
    'ü': 'u',
    'ý': 'y',
    'þ': 'th',
    'ÿ': 'y'
})

def _slugify_core(value):
    """
    Remove non-word symbols except for _ and -,
    then replace all hyphens with underscores,
    then replace all sequences of underscores and spaces with one underscore.
    """
    value = re.sub(r'[^\w\s_-]', '', value).strip().lower()
    value = re.sub(r'-', '_', value)
    return re.sub(r'[_\s]+', '_', value)

# NB: Originally stolen from Django, now quite mangled.
def slugify(value, allow_unicode=False, translate_latin=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to underscores.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.

    :param allow_unicode: If set to true, does not remove unicode.
    :param translate_latin: If set to true, maps LATIN-1 characters
                            to closest visual ascii representation.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
        if translate_latin:
            value = value.translate(LATIN_TRANSLATE)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    return _slugify_core(value)

def msnow():
    """Return the current time in milliseconds."""
    return int(time.time() * 1000)
