"""valuedispatch function decorator, as obtained from
http://lukasz.langa.pl/8/single-dispatch-generic-functions/"""

from functools import singledispatch
from functools import wraps
from enum import Enum

def valuedispatch(func):
    _func = singledispatch(func)

    @_func.register(Enum)
    def _enumvalue_dispatch(*args, **kw):
        enum, value = args[0], args[0].value
        if value not in _func.registry:
            return _func.dispatch(object)(*args, **kw)
        dispatch = _func.registry[value]
        _func.register(enum, dispatch)
        return dispatch(*args, **kw)

    @wraps(func)
    def wrapper(*args, **kw):
        if args[0] in _func.registry:
            return _func.registry[args[0]](*args, **kw)
        return _func(*args, **kw)

    def register(value):
        return lambda f: _func.register(value, f)

    wrapper.register = register
    wrapper.dispatch = _func.dispatch
    wrapper.registry = _func.registry
    return wrapper
