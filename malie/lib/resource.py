import os.path
import subprocess

import pkg_resources
import setuptools_scm

from malie.lib.cache import Cache


PKGNAME = 'malie'

def directory():
    return Cache(PKGNAME, data=True).basedir

def install_version():
    return pkg_resources.get_distribution(PKGNAME).version

def live_version():
    version = setuptools_scm.get_version(root='../..', relative_to=__file__)
    return version

def get_version():
    try:
        version = live_version()
    except LookupError:
        version = install_version()
    return version

def git_mode():
    try:
        setuptools_scm.get_version(root='../../', relative_to=__file__)
        return True
    except LookupError:
        return False

def get_resource(name, cache=None):
    """
    Given a resource name from MANIFESTS.in, return a filename for it.

    If the filename is determined to be in a git directory, that file path will
    be returned as-is, so that scripts running in development mode can update
    the packaged data files.

    If the filename is not in a git directory, it will be first copied to a
    local cache. That cache will default to $XDG_DATA_HOME/malie/;
    most likely ~/.local/share/malie/. The cache can be overridden by the caller.
    """

    fname = pkg_resources.resource_filename(f"{PKGNAME}.data", name)
    cwd = os.path.dirname(fname)
    basename = os.path.basename(fname)
    res = subprocess.run(['git', 'rev-parse', basename],
                         cwd=cwd,
                         stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # This file is in a git path
    if res.returncode == 0:
        return fname

    # This file is not in a git path; copy it lazily into cache
    if not cache:
        cache = Cache("malie", data=True)
    path = cache.copy(basename, fname)

    return path
