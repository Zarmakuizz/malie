PIP = pip3

.PHONY: help
help:
	@echo "~ alola ~"
	@echo
	@echo "install:     Use $(PIP) to install package."
	@echo "develop:     Use $(PIP) to install package in editable mode."
	@echo "userinstall: install, but to --user directory."
	@echo "userdevelop: develop, but to --user directory."
	@echo "distclean:   Remove all build and distribution files."

distclean:
	rm -rf malie.egg-info build/ dist/ .eggs/ malie/scrapers/ptcgo/ptcgo_pb2.py

.PHONY: unityfork
unityfork:
	$(PIP) install $(PIPFLAGS) 'git+https://gitlab.com/malie-library/unitypack.git@0.9.1a0#egg=unitypack'

.PHONY: install
install: unityfork
	$(PIP) $(PIPFLAGS) install .

.PHONY: develop
develop: unityfork
	$(PIP) $(PIPFLAGS) install -e .

.PHONY: userinstall
userinstall: PIPFLAGS += --user
userinstall: install

.PHONY: userdevelop
userdevelop: PIPFLAGS += --user
userdevelop: develop

.PHONY: pristine
pristine:
	@git diff-files --quiet --ignore-submodules -- || \
		(echo "You have unstaged changes."; exit 1)
	@git diff-index --cached --quiet HEAD --ignore-submodules -- || \
		(echo "Your index contains uncommitted changes."; exit 1)
	@[ -z "$(shell git ls-files -o --exclude-from=.pubignore)" ] || \
		(echo "You have untracked files: $(shell git ls-files -o --exclude-from=.pubignore)"; exit 1)

dist build:
	python3 setup.py sdist bdist_wheel

publish: distclean pristine dist build
	git push -v --follow-tags --dry-run
	git push --follow-tags
